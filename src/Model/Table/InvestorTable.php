<?php
namespace App\Model\Table;
use  Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;

Class  InvestorTable  extends Table
{
  public function initialize (array $config)
  {
$this->addBehavior('Timestamp');
}
public function beforeSave($event, $entity, $options)
{
if ($entity->isNew() && !$entity->slug) {
$sluggedTitle = Text::slug($entity->title);
// trim slug to maximum length defined in schema
$entity->slug = substr($sluggedTitle, 0, 191);
}
}
public function validationDefault(Validator $validator) 
{
	$validator
	->allowEmptyString('Id_saldo',false)
	->minLength('Id_saldo',10)
	->maxLength('Id_saldo',255)
	->allowEmptyString('Calificacion',false)
	->minLength('Calificacion',4);
	
	return $validator;
	
}
}
