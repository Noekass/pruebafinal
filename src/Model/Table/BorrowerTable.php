<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Borrower Model
 *
 * @method \App\Model\Entity\Borrower get($primaryKey, $options = [])
 * @method \App\Model\Entity\Borrower newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Borrower[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Borrower|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Borrower saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Borrower patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Borrower[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Borrower findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BorrowerTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('borrower');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Id')
            ->allowEmptyString('Id', 'create');

        $validator
            ->integer('Id_Data_Borrower')
            ->requirePresence('Id_Data_Borrower', 'create')
            ->allowEmptyString('Id_Data_Borrower', false);

        $validator
            ->scalar('Monto_Deceado')
            ->maxLength('Monto_Deceado', 255)
            ->requirePresence('Monto_Deceado', 'create')
            ->allowEmptyString('Monto_Deceado', false);

        return $validator;
    }
}
