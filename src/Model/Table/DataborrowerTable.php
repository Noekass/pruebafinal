<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Databorrower Model
 *
 * @method \App\Model\Entity\Databorrower get($primaryKey, $options = [])
 * @method \App\Model\Entity\Databorrower newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Databorrower[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Databorrower|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Databorrower saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Databorrower patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Databorrower[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Databorrower findOrCreate($search, callable $callback = null, $options = [])
 */
class DataborrowerTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('databorrower');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('Nombre_De_La_Empresa')
            ->maxLength('Nombre_De_La_Empresa', 255)
            ->requirePresence('Nombre_De_La_Empresa', 'create')
            ->allowEmptyString('Nombre_De_La_Empresa', false);

        $validator
            ->scalar('Puesto')
            ->maxLength('Puesto', 255)
            ->requirePresence('Puesto', 'create')
            ->allowEmptyString('Puesto', false);

        $validator
            ->scalar('Numero_Tel')
            ->maxLength('Numero_Tel', 255)
            ->requirePresence('Numero_Tel', 'create')
            ->allowEmptyString('Numero_Tel', false);

        $validator
            ->scalar('Id_Ususuario')
            ->maxLength('Id_Ususuario', 255)
            ->requirePresence('Id_Ususuario', 'create')
            ->allowEmptyString('Id_Ususuario', false);

        $validator
            ->scalar('Ingreso_Mensual')
            ->maxLength('Ingreso_Mensual', 255)
            ->requirePresence('Ingreso_Mensual', 'create')
            ->allowEmptyString('Ingreso_Mensual', false);

        $validator
            ->scalar('Afiliacion_IMSS')
            ->maxLength('Afiliacion_IMSS', 255)
            ->requirePresence('Afiliacion_IMSS', 'create')
            ->allowEmptyString('Afiliacion_IMSS', false);

        $validator
            ->scalar('Segur_Gastos_Mayores')
            ->maxLength('Segur_Gastos_Mayores', 255)
            ->requirePresence('Segur_Gastos_Mayores', 'create')
            ->allowEmptyString('Segur_Gastos_Mayores', false);

        $validator
            ->scalar('Gastos_Alimentacion')
            ->maxLength('Gastos_Alimentacion', 255)
            ->requirePresence('Gastos_Alimentacion', 'create')
            ->allowEmptyString('Gastos_Alimentacion', false);

        $validator
            ->scalar('Gatos_Renta')
            ->maxLength('Gatos_Renta', 255)
            ->requirePresence('Gatos_Renta', 'create')
            ->allowEmptyString('Gatos_Renta', false);

        $validator
            ->scalar('Gastos_Targeta_Deudas')
            ->maxLength('Gastos_Targeta_Deudas', 255)
            ->requirePresence('Gastos_Targeta_Deudas', 'create')
            ->allowEmptyString('Gastos_Targeta_Deudas', false);

        $validator
            ->scalar('Gastos_Educacion')
            ->maxLength('Gastos_Educacion', 255)
            ->requirePresence('Gastos_Educacion', 'create')
            ->allowEmptyString('Gastos_Educacion', false);

        $validator
            ->scalar('Gastos_Servicios')
            ->maxLength('Gastos_Servicios', 255)
            ->requirePresence('Gastos_Servicios', 'create')
            ->allowEmptyString('Gastos_Servicios', false);

        $validator
            ->scalar('Gastos_Transportes')
            ->maxLength('Gastos_Transportes', 255)
            ->requirePresence('Gastos_Transportes', 'create')
            ->allowEmptyString('Gastos_Transportes', false);

        $validator
            ->scalar('Gastos_Seguros')
            ->maxLength('Gastos_Seguros', 255)
            ->requirePresence('Gastos_Seguros', 'create')
            ->allowEmptyString('Gastos_Seguros', false);

        $validator
            ->scalar('Tenencia_Auto')
            ->maxLength('Tenencia_Auto', 255)
            ->requirePresence('Tenencia_Auto', 'create')
            ->allowEmptyString('Tenencia_Auto', false);

        $validator
            ->scalar('Calificacion')
            ->maxLength('Calificacion', 255)
            ->requirePresence('Calificacion', 'create')
            ->allowEmptyString('Calificacion', false);

        return $validator;
    }
}
