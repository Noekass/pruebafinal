<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;

Class FondeobancoTable extends Table
{
	public function initialize (array $config)
	{
	$this->addBehavior('Timestamp');
	}
	public function beforeSave($event,$entity,$options)
	{
		if ($entity->isNew() && !$entity->Id){
			//$sluggedTitle = Text::Id($entity->saldo);
			// trim slug to maximum length defined in chema
			//$entity->Id = substr($sluggedTitle, 0,191);
		}
	}
	public function validationDefault(Validator $validator){
		$validator
		->allowEmptyString('Saldo',false)
		->minLength('Saldo',1)
		->maxLength('Saldo',100);

		return $validator;

	}
}