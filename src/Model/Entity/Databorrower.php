<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Databorrower Entity
 *
 * @property int $id
 * @property string $Nombre_De_La_Empresa
 * @property string $Puesto
 * @property string $Numero_Tel
 * @property string $Id_Ususuario
 * @property string $Ingreso_Mensual
 * @property string $Afiliacion_IMSS
 * @property string $Segur_Gastos_Mayores
 * @property string $Gastos_Alimentacion
 * @property string $Gatos_Renta
 * @property string $Gastos_Targeta_Deudas
 * @property string $Gastos_Educacion
 * @property string $Gastos_Servicios
 * @property string $Gastos_Transportes
 * @property string $Gastos_Seguros
 * @property string $Tenencia_Auto
 * @property string $Calificacion
 */
class Databorrower extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'Nombre_De_La_Empresa' => true,
        'Puesto' => true,
        'Numero_Tel' => true,
        'Id_Ususuario' => true,
        'Ingreso_Mensual' => true,
        'Afiliacion_IMSS' => true,
        'Segur_Gastos_Mayores' => true,
        'Gastos_Alimentacion' => true,
        'Gatos_Renta' => true,
        'Gastos_Targeta_Deudas' => true,
        'Gastos_Educacion' => true,
        'Gastos_Servicios' => true,
        'Gastos_Transportes' => true,
        'Gastos_Seguros' => true,
        'Tenencia_Auto' => true,
        'Calificacion' => true
    ];
}
