<?php
namespace App\Model\Entity;
use  Cake\ORM\Entity;

class Article extends Entity
{
  protected $_accesible =[
       '*' =>true,
       'Id_Investor'=> false,
       'Pagos_Tiempo'=>false,
  ];
}
