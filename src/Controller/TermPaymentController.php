<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TermPayment Controller
 *
 * @property \App\Model\Table\TermPaymentTable $TermPayment
 *
 * @method \App\Model\Entity\TermPayment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TermPaymentController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $termPayment = $this->paginate($this->TermPayment);

        $this->set(compact('termPayment'));
    }

    /**
     * View method
     *
     * @param string|null $id Term Payment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $termPayment = $this->TermPayment->get($id, [
            'contain' => []
        ]);

        $this->set('termPayment', $termPayment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $termPayment = $this->TermPayment->newEntity();
        if ($this->request->is('post')) {
            $termPayment = $this->TermPayment->patchEntity($termPayment, $this->request->getData());
            if ($this->TermPayment->save($termPayment)) {
                $this->Flash->success(__('The term payment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The term payment could not be saved. Please, try again.'));
        }
        $this->set(compact('termPayment'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Term Payment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $termPayment = $this->TermPayment->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $termPayment = $this->TermPayment->patchEntity($termPayment, $this->request->getData());
            if ($this->TermPayment->save($termPayment)) {
                $this->Flash->success(__('The term payment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The term payment could not be saved. Please, try again.'));
        }
        $this->set(compact('termPayment'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Term Payment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $termPayment = $this->TermPayment->get($id);
        if ($this->TermPayment->delete($termPayment)) {
            $this->Flash->success(__('The term payment has been deleted.'));
        } else {
            $this->Flash->error(__('The term payment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
