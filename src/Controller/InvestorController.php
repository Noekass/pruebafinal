<?php
namespace App\Controller;
class InvestorController extends AppController
{
  public function index()
  {
    $this->loadComponent('Paginator');
    $articles = $this->Paginator->paginate($this->Investor->find());
    $this->set(compact('articles'));
  }
  public function view($slug = null)
{
$article = $this->Articles->findBySlug($slug)->firstOrFail();
$this->set(compact('article'));
}

public function edit($Calificacion)
{
$article = $this->Investor->findByCalificacion($Calificacion)->firstOrFail();
if ($this->request->is(['post', 'put'])) {
$this->Investor->patchEntity($article, $this->request->getData());
if ($this->Investor->save($article)) {
$this->Flash->success(__('Tu inversor a sido editado correctamente'));
return $this->redirect(['action' => 'index']);

}
$this->Flash->error(__('Imposible guardar tu registro como inversor.'));
}
$this->set('article', $article);
}


public function add()
{
$article = $this->Investor->newEntity();
if ($this->request->is('post')) {
$article = $this->Investor->patchEntity($article, $this->request->getData());
// Hardcoding the user_id is temporary, and will be removed later
// when we build authentication out.
$article->user_id = 1;
if ($this->Investor->save($article)) {
$this->Flash->success(__('Tu registro fue exitoso.'));
return $this->redirect(['action' => 'index']);
}
$this->Flash->error(__('Imposible guardar su registro.'));
}
$this->set('article', $article);
}

public function delete($Calificacion)
{
 $this->request->allowMethod(['post','delete']);
 $article = $this->Investor->findByCalificacion($Calificacion)->firstOrFail();
 if ($this->Investor->delete($article))
 {
 $this->Flash->success(__('Inversor correctamente borrado'));
return $this->redirect(['action' => 'index']);
 }
}


}
