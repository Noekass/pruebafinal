<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Borrower Controller
 *
 * @property \App\Model\Table\BorrowerTable $Borrower
 *
 * @method \App\Model\Entity\Borrower[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BorrowerController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $borrower = $this->paginate($this->Borrower);

        $this->set(compact('borrower'));
    }

    /**
     * View method
     *
     * @param string|null $id Borrower id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $borrower = $this->Borrower->get($id, [
            'contain' => []
        ]);

        $this->set('borrower', $borrower);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $borrower = $this->Borrower->newEntity();
        if ($this->request->is('post')) {
            $borrower = $this->Borrower->patchEntity($borrower, $this->request->getData());
            if ($this->Borrower->save($borrower)) {
                $this->Flash->success(__('The borrower has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The borrower could not be saved. Please, try again.'));
        }
        $this->set(compact('borrower'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Borrower id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $borrower = $this->Borrower->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $borrower = $this->Borrower->patchEntity($borrower, $this->request->getData());
            if ($this->Borrower->save($borrower)) {
                $this->Flash->success(__('The borrower has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The borrower could not be saved. Please, try again.'));
        }
        $this->set(compact('borrower'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Borrower id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $borrower = $this->Borrower->get($id);
        if ($this->Borrower->delete($borrower)) {
            $this->Flash->success(__('The borrower has been deleted.'));
        } else {
            $this->Flash->error(__('The borrower could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
