<?php
namespace App\Controller;

class FondeobancoController extends AppController
{
	public function index()
	{
		$this->loadComponent('Paginator');
		$fondeobanco=$this->Paginator->Paginate($this->Fondeobanco->find()); 
		$this->set(compact('fondeobanco'));
	}
	public function view($Id=null)
	{
		$fondeobanco=$this->Fondeobanco->findById($Id)->firstOrFail();
		$this->set(compact('fondeobanco'));
	}

	public function edit($Id){
		$fondeobanco=$this->Fondeobanco->findById($Id)->firstOrFail();
		if($this->request->is(['post','put'])){
			$this->Fondeobanco->patchEntity($fondeobanco,$this->request->getData());
			if($this->Fondeobanco->save($fondeobanco)){
				$this->Flash->success(__('Your Money has been saved.')); 
				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('Unable to update your money.'));
		}
		$this->set('fondeobanco', $fondeobanco);
	}

	public function delete($Id){
		$this->request->allowMethod(['post','delete']);
		$fondeobanco=$this->Fondeobanco->findById($Id)->firstOrFail();
		if($this->Fondeobanco->delete($fondeobanco)){
			$this->Flash->success(__('Your money has been deleted.')); 
				return $this->redirect(['action' => 'index']);
		}
	}

	public function add() { 
		$fondeobanco = $this->Fondeobanco->newEntity(); 
		if ($this->request->is('post')) { 
			$fondeobanco = $this->Fondeobanco->patchEntity($fondeobanco, $this->request->getData());
			// Hardcoding the user_id is temporary, and will be removed later // when we build authentication out. $fondeobanco->user_id = 1;
			if ($this->Fondeobanco->save($fondeobanco)) { 
				$this->Flash->success(__('Your money has been saved.')); 
				return $this->redirect(['action' => 'index']); 
			} 
			$this->Flash->error(__('Unable to add your money.'));
		} $this->set('fondeobanco', $fondeobanco);
	}
}
