<?php
namespace App\Controller;

class CompanyController extends AppController
{
	public function index()
	{

		$this->loadComponent('Paginator');
		$company = $this->Paginator->paginate($this->Company->find()); 
		$this->set(compact('company'));
	}
	public function view($Username = null)
{
		$company = $this->Company->findByUsername($Username)->firstOrFail();
		$this->set(compact('company'));
}


public function edit($Username)
{
	$company = $this->Company->findByUsername($Username)->firstOrFail();
	if($this->request->is(['post','put'])) {
		$this->Company->patchEntity($company,$this->request->getData());
			if($this->Company->save($company)) {
				$this->Flash->success(__('Datos Actualizados Exitosamente'));
				return $this->redirect(['action' => 'index']);
			}
			$this->Flash->error(__('Upss... Existe algun error en los campos'));

	}$this->set('company',$company);
}

public function add()
{
	$company =  $this->Company->newEntity();
	if ($this->request->is('post')){
		$company = $this->Company->patchEntity($company, $this->request->getData());

		
	
	if($this->Company->save($company)){
		$this->Flash->success(__('Nuevos Datos Guardados'));
		return $this->redirect(['action'=>'index']);
	}
	$this->Flash->error(__('Upps... Existe un error en los campos'));
}
	$this->set('company',$company);
}


public function delete($Username)
{
	$this->request->allowMethod(['post','delete']);
	$company = $this->Company->findByUsername($Username)->firstOrFail();
	if($this->Company->delete($company))
	{
		$this->Flash->success(__('Eliminado Exitosamente'));
				return $this->redirect(['action' => 'index']);
	}
}

}