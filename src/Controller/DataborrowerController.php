<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Databorrower Controller
 *
 * @property \App\Model\Table\DataborrowerTable $Databorrower
 *
 * @method \App\Model\Entity\Databorrower[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DataborrowerController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $databorrower = $this->paginate($this->Databorrower);

        $this->set(compact('databorrower'));
    }

    /**
     * View method
     *
     * @param string|null $id Databorrower id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $databorrower = $this->Databorrower->get($id, [
            'contain' => []
        ]);

        $this->set('databorrower', $databorrower);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $databorrower = $this->Databorrower->newEntity();
        if ($this->request->is('post')) {
            $databorrower = $this->Databorrower->patchEntity($databorrower, $this->request->getData());
            if ($this->Databorrower->save($databorrower)) {
                $this->Flash->success(__('The databorrower has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The databorrower could not be saved. Please, try again.'));
        }
        $this->set(compact('databorrower'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Databorrower id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $databorrower = $this->Databorrower->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $databorrower = $this->Databorrower->patchEntity($databorrower, $this->request->getData());
            if ($this->Databorrower->save($databorrower)) {
                $this->Flash->success(__('The databorrower has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The databorrower could not be saved. Please, try again.'));
        }
        $this->set(compact('databorrower'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Databorrower id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $databorrower = $this->Databorrower->get($id);
        if ($this->Databorrower->delete($databorrower)) {
            $this->Flash->success(__('The databorrower has been deleted.'));
        } else {
            $this->Flash->error(__('The databorrower could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
