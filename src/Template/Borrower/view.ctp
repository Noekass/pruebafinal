<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Borrower $borrower
 */
?>

<div class="borrower view large-9 medium-8 columns content">
    <h3><?= h($borrower->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Monto Deceado') ?></th>
            <td><?= h($borrower->Monto_Deceado) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($borrower->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Data Borrower') ?></th>
            <td><?= $this->Number->format($borrower->Id_Data_Borrower) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($borrower->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($borrower->modified) ?></td>
        </tr>
    </table>
</div>
