<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Borrower[]|\Cake\Collection\CollectionInterface $borrower
 */
?>

<div class="borrower index large-9 medium-8 columns content">
    <h3><?= __('Borrower') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Id_Data_Borrower') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Monto_Deceado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($borrower as $borrower): ?>
            <tr>
                <td><?= $this->Number->format($borrower->Id) ?></td>
                <td><?= $this->Number->format($borrower->Id_Data_Borrower) ?></td>
                <td><?= h($borrower->Monto_Deceado) ?></td>
                <td><?= h($borrower->created) ?></td>
                <td><?= h($borrower->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $borrower->Id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $borrower->Id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $borrower->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $borrower->Id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
    <h5><?= $this->Html->link(__('New Borrower'), ['action' => 'add']) ?></h5>
</div>
