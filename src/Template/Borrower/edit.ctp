<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Borrower $borrower
 */
?>

<div class="borrower form large-9 medium-8 columns content">
    <?= $this->Form->create($borrower) ?>
    <fieldset>
        <legend><?= __('Edit Borrower') ?></legend>
        <?php
            echo $this->Form->control('Id_Data_Borrower');
            echo $this->Form->control('Monto_Deceado');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
