<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body> 
    <nav class='large-1.5 medium-2 columns' id='actions-sidebar'>
        <ul class='side-nav'>
               <b><h3><a href=""><?= $this->fetch('title') ?></a></h3></b>
            </li>
        </ul>
        <div class='large-3 medium-4 columns' id='actions-sidebar'>
            <ul class="side-nav">
               <li><a target="_blank" href="http://localhost/ProyectoFinal/investor">Investor</a></li>
               <li><a target="_blank" href="http://localhost/ProyectoFinal/Fondeobanco">Fondeo Banco</a></li>
               <li><a target="_blank" href="http://localhost/ProyectoFinal/borrower">Borrower</a></li>
               <li><a target="_blank" href="http://localhost/ProyectoFinal/databorrower">DataBorrower</a></li>
               
               <li><a target="_blank" href="http://localhost/ProyectoFinal/user">User</a></li>
               <li><a target="_blank" href="http://localhost/ProyectoFinal/TermPayment">Term Payment</a></li>
            </ul>
        </div>
    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>

    <footer class="fooder py-2 text-xs-center">
        <div >
        <div class="row">
        	 <div class="col-md-2">
                
                <ul >
                    
                <br>
                     <h5 class="Noe"></h5>
                </ul>
                </div>
            <div class="col-md-3">
                
                <ul >
                    
                <br>
                     <h5 class="Noe">Av, Mariano Otero 2347 Colonia Verde Valle, Sombrerete, Zacatecas.</h5>
                </ul>
                
            </div>
        <div class="col-md-2">
            <ul>
                
                <br>
                <h5 class="Noe" >+52 (433) 1234567</h5>
                <h5 class="Noe">info@invietecampo.com</h5>
            </ul>
            </div>
            <div class="col-md-2">
                <ul>
                    
                    <br>
                    <h5 class="Noe">Terminos y condiciones</h5>
                    <h5 class="Noe">Avisos de privasidad</h5>
                </ul>
            </div>
            <div class="col-md-3">
                    
                    <br>
                    <h5 class="Noe">D.R 2019 Todos los Derechos Reservados Comunidad de préstamos SAPI de CV</h5>
                    
                    
                </ul>
            </div>
            
        </div>
        </div>
    </footer>
</body>
</html>
