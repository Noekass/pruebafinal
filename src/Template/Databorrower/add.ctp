<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Databorrower $databorrower
 */
?>

<div class="databorrower form large-9 medium-8 columns content">
    <?= $this->Form->create($databorrower) ?>
    <fieldset>
        <legend><?= __('Add Databorrower') ?></legend>
        <?php
            echo $this->Form->control('Nombre_De_La_Empresa');
            echo $this->Form->control('Puesto');
            echo $this->Form->control('Numero_Tel');
            echo $this->Form->control('Id_Ususuario');
            echo $this->Form->control('Ingreso_Mensual');
            echo $this->Form->control('Afiliacion_IMSS');
            echo $this->Form->control('Segur_Gastos_Mayores');
            echo $this->Form->control('Gastos_Alimentacion');
            echo $this->Form->control('Gatos_Renta');
            echo $this->Form->control('Gastos_Targeta_Deudas');
            echo $this->Form->control('Gastos_Educacion');
            echo $this->Form->control('Gastos_Servicios');
            echo $this->Form->control('Gastos_Transportes');
            echo $this->Form->control('Gastos_Seguros');
            echo $this->Form->control('Tenencia_Auto');
            echo $this->Form->control('Calificacion');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
