<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Databorrower $databorrower
 */
?>

<div class="databorrower view large-9 medium-8 columns content">
    <h3><?= h($databorrower->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre De La Empresa') ?></th>
            <td><?= h($databorrower->Nombre_De_La_Empresa) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Puesto') ?></th>
            <td><?= h($databorrower->Puesto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numero Tel') ?></th>
            <td><?= h($databorrower->Numero_Tel) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Ususuario') ?></th>
            <td><?= h($databorrower->Id_Ususuario) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ingreso Mensual') ?></th>
            <td><?= h($databorrower->Ingreso_Mensual) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Afiliacion IMSS') ?></th>
            <td><?= h($databorrower->Afiliacion_IMSS) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Segur Gastos Mayores') ?></th>
            <td><?= h($databorrower->Segur_Gastos_Mayores) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gastos Alimentacion') ?></th>
            <td><?= h($databorrower->Gastos_Alimentacion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gatos Renta') ?></th>
            <td><?= h($databorrower->Gatos_Renta) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gastos Targeta Deudas') ?></th>
            <td><?= h($databorrower->Gastos_Targeta_Deudas) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gastos Educacion') ?></th>
            <td><?= h($databorrower->Gastos_Educacion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gastos Servicios') ?></th>
            <td><?= h($databorrower->Gastos_Servicios) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gastos Transportes') ?></th>
            <td><?= h($databorrower->Gastos_Transportes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Gastos Seguros') ?></th>
            <td><?= h($databorrower->Gastos_Seguros) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tenencia Auto') ?></th>
            <td><?= h($databorrower->Tenencia_Auto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Calificacion') ?></th>
            <td><?= h($databorrower->Calificacion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($databorrower->id) ?></td>
        </tr>
    </table>
</div>
