<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Databorrower[]|\Cake\Collection\CollectionInterface $databorrower
 */
?>

<div class="databorrower index large-12 medium-10 columns content">
    <h3><?= __('Databorrower') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Nombre_De_La_Empresa') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Puesto') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Numero_Tel') ?></th>
                
                
                <th scope="col"><?= $this->Paginator->sort('Segur_Gastos_Mayores') ?></th>
               
                <th scope="col"><?= $this->Paginator->sort('Gastos_Targeta_Deudas') ?></th>


                <th scope="col"><?= $this->Paginator->sort('Calificacion') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($databorrower as $databorrower): ?>
            <tr>
                <td><?= $this->Number->format($databorrower->id) ?></td>
                <td><?= h($databorrower->Nombre_De_La_Empresa) ?></td>
                <td><?= h($databorrower->Puesto) ?></td>
                <td><?= h($databorrower->Numero_Tel) ?></td>
                
                <td><?= h($databorrower->Ingreso_Mensual) ?></td>
               
                
                <td><?= h($databorrower->Gastos_Targeta_Deudas) ?></td>


                <td><?= h($databorrower->Calificacion) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $databorrower->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $databorrower->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $databorrower->id], ['confirm' => __('Are you sure you want to delete # {0}?', $databorrower->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
    <h5><?= $this->Html->link(__('New Databorrower'), ['action' => 'add']) ?></h5>
</div>
