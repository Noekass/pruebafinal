<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TermPayment $termPayment
 */
?>

<div class="termPayment form large-9 medium-8 columns content">
    <?= $this->Form->create($termPayment) ?>
    <fieldset>
        <legend><?= __('Add Term Payment') ?></legend>
        <?php
            echo $this->Form->control('id_investor');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
