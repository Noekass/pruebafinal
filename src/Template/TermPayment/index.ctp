<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TermPayment[]|\Cake\Collection\CollectionInterface $termPayment
 */
?>

<div class="termPayment index large-9 medium-8 columns content">
    <h3><?= __('Term Payment') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('id_investor') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($termPayment as $termPayment): ?>
            <tr>
                <td><?= $this->Number->format($termPayment->id) ?></td>
                <td><?= $this->Number->format($termPayment->id_investor) ?></td>
                <td><?= h($termPayment->created) ?></td>
                <td><?= h($termPayment->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $termPayment->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $termPayment->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $termPayment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $termPayment->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
     <h5><?= $this->Html->link(__('New Term Payment'), ['action' => 'add']) ?></h5>
</div>
