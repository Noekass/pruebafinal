<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TermPayment $termPayment
 */
?>

<div class="termPayment view large-9 medium-8 columns content">
    <h3><?= h($termPayment->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($termPayment->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Investor') ?></th>
            <td><?= $this->Number->format($termPayment->id_investor) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($termPayment->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($termPayment->modified) ?></td>
        </tr>
    </table>
</div>
