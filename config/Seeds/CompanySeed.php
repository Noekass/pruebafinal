<?php
use Migrations\AbstractSeed;

/**
 * Company seed.
 */
class CompanySeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	'Username' => 'Noe',
        	'lugar' => 'Sombrerete, Zacatecas',
        	'Telefono' => '+52 4331234567',
        	'Link_FB' => 'https://www.facebook.com/yotepresto/',
        	'Correo' => 'info@yotepresto.com',
        	'extras' => 'Av, Mariano Otero 2347 Colonia Verde Valle, Sombrerete, Zacatecas.'
        ];

        $table = $this->table('Company');
        $table->insert($data)->save();
    }
}
