<?php
use Migrations\AbstractMigration;

class CreateCompany extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('company');
        $table->addColumn('Username', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('lugar', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('Telefono', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('Link_FB', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('Correo', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('extras', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
